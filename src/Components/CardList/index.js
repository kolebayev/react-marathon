import React, { Component } from 'react';
import Card from '../Card';

import s from './CardList.module.scss';

class CardList extends Component {
  state = {
    value: '',
    label: '',
    //
    //
    // вопрос 2
    // почему не работает нестед стэйт?
    // три пары ключ-значение ниже мне было бы удобнее
    // обрабатывать цельниым объектом:
    // newItem: {
    //     id: 'id',
    //     rus: 'rus',
    //     eng: 'eng'
    // }
    // но обращение через newItem.id в сет стейте не работает
    //
    //

    // newItemId: '',
    // newItemRus: '',
    // newItemEng: '',
    newItem: {
      id: '',
      rus: '',
      eng: '',
    },
  };

  handleInputChange = (e) => {
    this.setState({
      value: e.target.value,
    });
  };

  handleSubmitForm = (e) => {
    e.preventDefault();
    this.setState(({ value }) => {
      return {
        label: value,
        value: '',
      };
    });
  };

  handleAddNewCardForm = (e) => {
    e.preventDefault();
    // this.props.addNewCard({
    //   id: `${Math.random() * 1000}`,
    //   eng: this.state.newItemEng,
    //   rus: this.state.newItemRus,
    // });
    this.props.addNewCard(this.state.newItem);
  };

  handleInputChangeRus = (e) => {
    this.setState({
    // newItemRus:  e.target.value ,
      newItem[rus]: e.target.value ,
    });
  };

  handleInputChangeEng = (e) => {
    this.setState({
        // newItemEng:e.target.value, 
      newItem.eng: e.target.value,
    });
  };

  render() {
    const { items } = this.props;

    return (
      <>
        <div>{this.state.label}</div>
        <form onSubmit={this.handleSubmitForm} style={{ marginBottom: '30px' }}>
          <input type="text" value={this.state.value} onChange={this.handleInputChange} />
          <button>add new word</button>
        </form>

        <form onSubmit={this.handleAddNewCardForm} style={{ marginBottom: '30px' }}>
          <input placeholder="in russian" onChange={this.handleInputChangeRus} type="text" />
          <input placeholder="in english" onChange={this.handleInputChangeEng} type="text" />
          <button>add new card</button>
        </form>

        <div className={s.root}>
          {items.map(({ eng, rus, id }) => (
            <Card
              key={id}
              eng={eng}
              rus={rus}
              handleDeleteClick={() => {
                this.props.delete(id);
              }}
            />
          ))}
        </div>
      </>
    );
  }
}

export default CardList;
